# Segregation of Duties Demo for cross-project workflow

This project doesn't push code all the way to production. 


### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

Changes for the SoD Demo includes upgrading to a much newer spring and adding a configuration for port exposure. 

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

This repo has a CI Pipeline that overrides and suppresses several jobs. It also has a new job for hand-off to create an MR and a separate job to commit the reference to the new artifacts. 
